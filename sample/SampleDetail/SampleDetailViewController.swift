//
//  SampleDetailDetailViewController.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation

class SampleDetailViewController: SampleDetailViewControllerBuilder{
    
    var presenter: VTPSampleDetailProtocol?
    var dataSampleDetail : SampleEntity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        userIdLabel.text = "\(dataSampleDetail?.userID ?? 0)"
        idLabel.text = "\(dataSampleDetail?.id ?? 0)"
        titleLabel.text = dataSampleDetail?.title
        bodyLabel.text = dataSampleDetail?.body
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    
}
extension SampleDetailViewController: PTVSampleDetailProtocol{
    
    
}
