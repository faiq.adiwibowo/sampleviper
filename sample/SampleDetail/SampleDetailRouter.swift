//
//  SampleDetailDetailRouter.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation
import UIKit

class SampleDetailRouter: PTRSampleDetailProtocol {
    
    var navigationController: UINavigationController?
    
    static func createAdditionalInformationModule() -> SampleDetailViewController {
        let view = SampleDetailViewController()
        
        let presenter = SampleDetailPresenter()
        let interactor: PTISampleDetailProtocol = SampleDetailInteractor()
        let router:PTRSampleDetailProtocol = SampleDetailRouter()
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        view.presenter = presenter
         
        return view
    }
    
}
