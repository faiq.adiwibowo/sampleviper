//
//  SampleDetailProtocol.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation
import UIKit

protocol VTPSampleDetailProtocol: AnyObject{
    
    var view: PTVSampleDetailProtocol? {get set}
    var interactor: PTISampleDetailProtocol? {get set}
    var router: PTRSampleDetailProtocol? {get set}
    
}

protocol PTVSampleDetailProtocol: AnyObject{
    
}

protocol PTISampleDetailProtocol: AnyObject{
    var presenter:ITPSampleDetailProtocol? {get set}
    
    
}
protocol PTRSampleDetailProtocol: AnyObject{
    
}
protocol ITPSampleDetailProtocol: AnyObject{
    
}
