//
//  SampleDetailViewControllerBuilder.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation
import UIKit

class SampleDetailViewControllerBuilder: BaseViewController{
    
    var tableView : UITableView!
    var cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        view.backgroundColor = .red
        setupLayout()
    }
    
    func setupLayout(){
        
        view.addSubview(userIdLabel)
        view.addSubview(idLabel)
        view.addSubview(titleLabel)
        view.addSubview(bodyLabel)
        
        NSLayoutConstraint.activate([
        
            userIdLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 50),
            userIdLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            userIdLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            idLabel.topAnchor.constraint(equalTo: userIdLabel.bottomAnchor, constant: 15),
            idLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15),
            idLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: idLabel.bottomAnchor, constant: 15),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            bodyLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 15),
            bodyLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15),
            bodyLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
        
        
    }
    
    let userIdLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.programatically()
        return label
    }()
    let idLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.programatically()
        return label
    }()
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.programatically()
        return label
    }()
    let bodyLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.programatically()
        return label
    }()
}


