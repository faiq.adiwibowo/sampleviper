//
//  SampleDetailDetailPresenter.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation
class SampleDetailPresenter: VTPSampleDetailProtocol{
    
    var view: PTVSampleDetailProtocol?
    
    var interactor: PTISampleDetailProtocol?
    
    var router: PTRSampleDetailProtocol?
    
}
extension SampleDetailPresenter: ITPSampleDetailProtocol{

}
