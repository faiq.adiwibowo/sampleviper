//
//  UIKitExtension.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation
import UIKit

extension UIView {
    
    func programatically(){
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
}
