//
//  BaseBuilder.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    var viewWidth = 0.0
    var viewHeight = 0.0
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewWidth = view.frame.width
        viewHeight = view.frame.height
        
        view.backgroundColor = .white
        
        setupLayoutView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setupLayoutView(){
        
    }
    
}
