//
//  SampleRouter.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation
import UIKit

class SampleRouter: PTRSampleProtocol {
    
    var navigationController: UINavigationController?
    
    static func createAdditionalInformationModule() -> SampleViewController {
        let view = SampleViewController()
        
        let presenter = SamplePresenter()
        let interactor: PTISampleProtocol = SampleInteractor()
        let router:PTRSampleProtocol = SampleRouter()
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        view.presenter = presenter
         
        return view
    }
    
}
