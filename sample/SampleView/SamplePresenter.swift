//
//  SamplePresenter.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation

class SamplePresenter: VTPSampleProtocol{
    
    var view: PTVSampleProtocol?
    
    var interactor: PTISampleProtocol?
    
    var router: PTRSampleProtocol?
    
    func callApi(){
        interactor?.callApi()
    }
    
}
extension SamplePresenter: ITPSampleProtocol{
    func getSampleDataSuccess(sampleDataList: [SampleEntity]){
        view?.getSampleDataSuccess(sampleDataList: sampleDataList)
    }
    func getSampleDataFailed(message: String){
        view?.getSampleDataFailed(message: message)
    }
}
