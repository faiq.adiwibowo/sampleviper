//
//  SampleViewController.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation
import UIKit


class SampleViewController: SampleViewControllerBuilder{
    
    var presenter: VTPSampleProtocol?
    var dataSample = [SampleEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        presenter?.callApi()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    
}
extension SampleViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSample.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        cell?.contentView.isUserInteractionEnabled = false
        cell?.preservesSuperviewLayoutMargins = false
        cell?.separatorInset = UIEdgeInsets.zero
        cell?.layoutMargins = UIEdgeInsets.zero
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? SampleCell else {return}
        cell.labelTitle.text = dataSample[indexPath.row].title
        cell.labelBody.text = dataSample[indexPath.row].body
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row index = \(indexPath.row)")
        let vc = SampleDetailRouter.createAdditionalInformationModule()
        vc.dataSampleDetail = dataSample[indexPath.row]
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .coverVertical
        present(vc, animated: true)
    }
    
}
extension SampleViewController: PTVSampleProtocol{
    func getSampleDataSuccess(sampleDataList: [SampleEntity]){
        dataSample = sampleDataList
        tableView.reloadData()
    }
    func getSampleDataFailed(message: String){
        print("failed message = \(message)")
    }
    //
    
}
