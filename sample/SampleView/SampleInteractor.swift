//
//  SampleInteractor.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation
import Alamofire

class SampleInteractor: PTISampleProtocol{
    
    var presenter: ITPSampleProtocol?
    var dataSample = [SampleEntity]()
    
    func callApi() {
        
        if let url = URL(string: "https://jsonplaceholder.typicode.com/posts") {
            AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, interceptor: .none, requestModifier: .none)
                .responseJSON { (request) in
                    switch request.result{
                    case .success(let json):
                        //
                        print(json)
                        
                        do {
                            let decoder = JSONDecoder()
                            
                            if let data = request.data{
                                print(data)
                                self.dataSample = try decoder.decode([SampleEntity].self, from: data)
                                print("VALUE RESPONSE = \(self.dataSample)")
                                self.presenter?.getSampleDataSuccess(sampleDataList: self.dataSample ?? [SampleEntity]())
                            }
                            
                        } catch let error {
                            print("VALUE ERROR")
//                            self.presenter?.getSampleDataFailed(message: error)
                        }
                        
                    case .failure(let error):
                        
                        print(error)
                        print("CALL API FAIL")
//                        self.presenter?.getSampleDataFailed(message: error)
                    }
                }
        }
    }
}
