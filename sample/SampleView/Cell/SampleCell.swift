//
//  SampleCell.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation
import UIKit

class SampleCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupLayoutCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLayoutCell(){
        addSubview(containerCell)
        addSubview(labelTitle)
        addSubview(labelBody)
        
        NSLayoutConstraint.activate([
            containerCell.topAnchor.constraint(equalTo: topAnchor),
            containerCell.bottomAnchor.constraint(equalTo: bottomAnchor),
            containerCell.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerCell.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            labelTitle.topAnchor.constraint(equalTo: containerCell.topAnchor, constant: 5),
            labelTitle.leadingAnchor.constraint(equalTo: containerCell.leadingAnchor, constant: 8),
            labelTitle.trailingAnchor.constraint(equalTo: containerCell.trailingAnchor, constant: -8),
            
            labelBody.topAnchor.constraint(equalTo: labelTitle.bottomAnchor, constant: 5),
            labelBody.leadingAnchor.constraint(equalTo: labelBody.leadingAnchor),
            labelBody.trailingAnchor.constraint(equalTo: labelBody.trailingAnchor),
            labelBody.bottomAnchor.constraint(equalTo: containerCell.bottomAnchor, constant: -5)
        ])
    }
    
    let containerCell : UIView = {
        let view = UIView()
        view.programatically()
        return view
    }()
    let labelTitle: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 12)
        label.programatically()
        return label
    }()
    let labelBody: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12)
        label.programatically()
        return label
    }()
}
