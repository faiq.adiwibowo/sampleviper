//
//  SampleViewControllerBuilder.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation
import UIKit

class SampleViewControllerBuilder: BaseViewController{
    
    var tableView : UITableView!
    var cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        view.backgroundColor = .red
        setupLayout()
    }
    
    func setupLayout(){
        tableView = UITableView()
        tableView.programatically()
        
        tableView.register(SampleCell.self, forCellReuseIdentifier: cellId)
        tableView.separatorStyle = .singleLine
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.layer.borderWidth = 1
        tableView.layer.cornerRadius = 5
        
        view.addSubview(titleHeader)
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            titleHeader.topAnchor.constraint(equalTo: view.topAnchor, constant: 5),
            titleHeader.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 15),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        
    }
    
    let titleHeader: UILabel = {
        let label = UILabel()
        label.text = "SAMPLE LIST"
        label.programatically()
        return label
    }()
    
}

