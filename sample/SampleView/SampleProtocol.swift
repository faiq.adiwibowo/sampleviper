//
//  SampleProtocol.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation
import UIKit

protocol VTPSampleProtocol: AnyObject{
    
    var view: PTVSampleProtocol? {get set}
    var interactor: PTISampleProtocol? {get set}
    var router: PTRSampleProtocol? {get set}
    
    func callApi()
}

protocol PTVSampleProtocol: AnyObject{
    func getSampleDataSuccess(sampleDataList: [SampleEntity])
    func getSampleDataFailed(message: String)
}

protocol PTISampleProtocol: AnyObject{
    var presenter:ITPSampleProtocol? {get set}
    
    func callApi()
    
}
protocol PTRSampleProtocol: AnyObject{
    
}
protocol ITPSampleProtocol: AnyObject{
    func getSampleDataSuccess(sampleDataList: [SampleEntity])
    func getSampleDataFailed(message: String)
}
