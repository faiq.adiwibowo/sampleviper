//
//  SampleEntiry.swift
//  sample
//
//  Created by faiq adi on 27/02/23.
//

import Foundation


struct SampleEntity: Codable {
    let userID, id: Int
    let title, body: String

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}

